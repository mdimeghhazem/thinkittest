import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TestService {
  tab: any[];
  constructor(
    private client: HttpClient
  ) { }


   getData(): Observable<any> {     
     return this.client.get('https://hjcgmxfb9b.execute-api.eu-central-1.amazonaws.com/dev/generate', {
        headers: {'x-thinkit-custom': 'hazem'}
      });
    }

}
