import { Component, OnInit } from '@angular/core';
import { TestService } from './test.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'thinkitTest';
    tab : any[] =[] ;
    transformed: any[]= [];
    constructor(
      private service:TestService
    ) {}


      transform(tab: number[]): number[] {
        let t: number[] = [];
        tab.forEach((i, index) => {
          let s: number = 1;
          tab.forEach((j,x)=> {
            if (x != index) {
              s=s*j;
            }
          } )
          t[index]=s;
        })
        return t;
      }

    ngOnInit() {
       this.transformed=this.transform(this.tab);
      console.log(this.transformed);
      this.service.getData().subscribe(
        res => {
          this.tab = res.randomInput; 
          console.log(res)         
          this.transformed = this.transform(this.tab);
          
        }, error => {
          console.log(error);
        }
      )
    }

}
